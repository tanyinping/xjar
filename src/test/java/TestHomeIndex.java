import io.xjar.XEntryFilter;
import io.xjar.XKit;
import io.xjar.boot.XBoot;
import io.xjar.jar.XJar;
import io.xjar.key.XKey;
import io.xjar.util.MACUtils;
import io.xjar.util.PropertiesUtil;
import io.xjar.util.RSAUtil;
import org.apache.commons.compress.archivers.jar.JarArchiveEntry;

import java.security.KeyPair;
import java.util.Arrays;
import java.util.Properties;

/**
 * @author fish
 * @since 2020/4/5 3:14 下午
 */
public class TestHomeIndex {

    private static final String BASE_PATH = "/Users/fish/Documents/workspace/self-projects/xjar/src/test/resources/appJarDir/";

    // 数据库分析系统
    private static final String APP_DB_ANALYSIS_SYS = "jts";

    public static void main(String[] args) throws Exception {

//        generatePSAKeyPair();
//        encryptJar(APP_DB_ANALYSIS_SYS);
//        encryptSpringBootJar(APP_DB_ANALYSIS_SYS);
//        String macSet = MACUtils.getMACSet();
//        System.out.println(macSet);
    }


    // 生成密钥对
    public static void generatePSAKeyPair(){
        KeyPair keyPair = RSAUtil.generateRSAKeyPair();
        String rsaPublicKeyModulusStr = RSAUtil.getRSAPublicKeyModulusStr(keyPair);
        String rsaPrivateKeyModulusStr = RSAUtil.getRSAPrivateKeyModulusStr(keyPair);
        System.out.println("公钥："+rsaPublicKeyModulusStr);
        System.out.println("私钥："+rsaPrivateKeyModulusStr);
    }

    // 加密
    public static void encryptJar(String appJarName) throws Exception {
        Properties properties = PropertiesUtil.loadProperties("/"+appJarName+"_rsaKey.properties");
        String password = properties.getProperty("password");
        String endDate = properties.getProperty("endDate");
        String rsaPublicKeyModulusStr = properties.getProperty("rsaPublicKey");
        String rsaPrivateKeyModulusStr = properties.getProperty("rsaPrivateKey");

        XKey fishXKey = XKit.fishKey(password, endDate,"", rsaPublicKeyModulusStr, rsaPrivateKeyModulusStr);

        String sourceJar = BASE_PATH+appJarName+".jar";
        String encryptedJar = BASE_PATH+appJarName+"-encrypted.jar";
        XJar.encrypt(sourceJar, encryptedJar, fishXKey);

    }



    // 加密
    public static void encryptSpringBootJar(String appJarName) throws Exception {
        Properties properties = PropertiesUtil.loadProperties("/"+appJarName+"_rsaKey.properties");
        String password = properties.getProperty("password");
        String endDate = properties.getProperty("endDate");
        String rsaPublicKeyModulusStr = properties.getProperty("rsaPublicKey");
        String rsaPrivateKeyModulusStr = properties.getProperty("rsaPrivateKey");

        XKey fishXKey = XKit.fishKey(password, endDate,"", rsaPublicKeyModulusStr, rsaPrivateKeyModulusStr);

        String sourceJar = BASE_PATH+appJarName+".jar";
        String encryptedJar = BASE_PATH+"encrtpted/"+appJarName+".jar";

//        XEntryFilter<JarArchiveEntry> not = XKit.not(new XJarAntEntryFilter("BOOT-INF/classes/static/**"));
//        XBoot.encrypt(sourceJar, encryptedJar, fishXKey);


        XBoot.encrypt(
                sourceJar,
                encryptedJar,
                fishXKey,
                new XEntryFilter<JarArchiveEntry>() {
                    @Override
                    public boolean filtrate(JarArchiveEntry entry) {
                        String name = entry.getName();
                        return !name.startsWith("static/");
                    }
                }
        );

    }

    // 解密
    public static void decryptJar(String appJarName) throws Exception {
        Properties properties = PropertiesUtil.loadProperties("/"+appJarName+"_rsaKey.properties");
        String password = properties.getProperty("password");
        String endDate = properties.getProperty("endDate");
        String rsaPublicKeyModulusStr = properties.getProperty("rsaPublicKey");
        String rsaPrivateKeyModulusStr = properties.getProperty("rsaPrivateKey");

        XKey fishXKey = XKit.fishKey(password, endDate,"", rsaPublicKeyModulusStr, rsaPrivateKeyModulusStr);

        String sourceJar = BASE_PATH+appJarName+"-encrypted.jar";
        String encryptedJar = BASE_PATH+appJarName+"-decrypted.jar";
        XJar.decrypt(sourceJar, encryptedJar, fishXKey);

    }


}
