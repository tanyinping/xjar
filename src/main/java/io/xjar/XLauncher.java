package io.xjar;

import io.xjar.key.FishXSymmetricSecureKey;
import io.xjar.key.XKey;
import io.xjar.util.MACUtils;
import io.xjar.util.RSAUtil;

import java.io.*;
import java.net.URI;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

/**
 * Spring-Boot 启动器
 *
 * @author Payne 646742615@qq.com
 * 2019/4/14 10:28
 */
public class XLauncher implements XConstants {
    public final String[] args;
    public final XDecryptor xDecryptor;
    public final XEncryptor xEncryptor;
    public final XKey xKey;

    public XLauncher(String... args) throws Exception {
        this.args = args;
        String algorithm = DEFAULT_ALGORITHM;
        int keysize = DEFAULT_KEYSIZE;
        int ivsize = DEFAULT_IVSIZE;
        String password = null;
        String keypath = null;
        String fishKey = null;
        String encryptedEndDate = null;
        String encryptedMacAddr = null;
        String rsaPublicKey = null;
        String rsaPrivateKey = null;
        for (String arg : args) {
            if (arg.toLowerCase().startsWith(XJAR_ALGORITHM)) {
                algorithm = arg.substring(XJAR_ALGORITHM.length());
            }
            if (arg.toLowerCase().startsWith(XJAR_KEYSIZE)) {
                keysize = Integer.valueOf(arg.substring(XJAR_KEYSIZE.length()));
            }
            if (arg.toLowerCase().startsWith(XJAR_IVSIZE)) {
                ivsize = Integer.valueOf(arg.substring(XJAR_IVSIZE.length()));
            }
            if (arg.toLowerCase().startsWith(XJAR_PASSWORD)) {
                password = arg.substring(XJAR_PASSWORD.length());
            }
            if (arg.toLowerCase().startsWith(XJAR_KEYFILE)) {
                keypath = arg.substring(XJAR_KEYFILE.length());
            }
            if (arg.startsWith(FishXSymmetricSecureKey.FISH_KEY)) {
                fishKey = arg.substring(FishXSymmetricSecureKey.FISH_KEY.length());
            }
            if (arg.startsWith(FishXSymmetricSecureKey.ENCRYPTED_END_DATE)) {
                encryptedEndDate = arg.substring(FishXSymmetricSecureKey.ENCRYPTED_END_DATE.length());
            }
            if (arg.startsWith(FishXSymmetricSecureKey.ENCRYPTED_MAC_ADDR)) {
                encryptedMacAddr = arg.substring(FishXSymmetricSecureKey.ENCRYPTED_MAC_ADDR.length());
            }
            if (arg.startsWith(FishXSymmetricSecureKey.RSA_PUBLIC_KEY)) {
                rsaPublicKey = arg.substring(FishXSymmetricSecureKey.RSA_PUBLIC_KEY.length());
            }
            if (arg.startsWith(FishXSymmetricSecureKey.RSA_PRIVATE_KEY)) {
                rsaPrivateKey = arg.substring(FishXSymmetricSecureKey.RSA_PRIVATE_KEY.length());
            }
        }

        ProtectionDomain domain = this.getClass().getProtectionDomain();
        CodeSource source = domain.getCodeSource();
        URI location = (source == null ? null : source.getLocation().toURI());
        String filepath = (location == null ? null : location.getSchemeSpecificPart());
        if (filepath != null) {
            File file = new File(filepath);
            JarFile jar = new JarFile(file, false);
            Manifest manifest = jar.getManifest();
            Attributes attributes = manifest.getMainAttributes();
            if (attributes.getValue(XJAR_ALGORITHM_KEY) != null) {
                algorithm = attributes.getValue(XJAR_ALGORITHM_KEY);
            }
            if (attributes.getValue(XJAR_KEYSIZE_KEY) != null) {
                keysize = Integer.valueOf(attributes.getValue(XJAR_KEYSIZE_KEY));
            }
            if (attributes.getValue(XJAR_IVSIZE_KEY) != null) {
                ivsize = Integer.valueOf(attributes.getValue(XJAR_IVSIZE_KEY));
            }
            if (attributes.getValue(XJAR_PASSWORD_KEY) != null) {
                password = attributes.getValue(XJAR_PASSWORD_KEY);
            }

            if (attributes.getValue(FishXSymmetricSecureKey.FISH_KEY) != null) {
                fishKey = attributes.getValue(FishXSymmetricSecureKey.FISH_KEY);
            }
            if (attributes.getValue(FishXSymmetricSecureKey.ENCRYPTED_END_DATE) != null) {
                encryptedEndDate = attributes.getValue(FishXSymmetricSecureKey.ENCRYPTED_END_DATE);
            }
            if (attributes.getValue(FishXSymmetricSecureKey.ENCRYPTED_MAC_ADDR) != null) {
                encryptedMacAddr = attributes.getValue(FishXSymmetricSecureKey.ENCRYPTED_MAC_ADDR);
            }
            if (attributes.getValue(FishXSymmetricSecureKey.RSA_PUBLIC_KEY) != null) {
                rsaPublicKey = attributes.getValue(FishXSymmetricSecureKey.RSA_PUBLIC_KEY);
            }
            if (attributes.getValue(FishXSymmetricSecureKey.RSA_PRIVATE_KEY) != null) {
                rsaPrivateKey = attributes.getValue(FishXSymmetricSecureKey.RSA_PRIVATE_KEY);
            }
        }

        Properties key = null;
        File keyfile = null;
        if (keypath != null) {
            String path = XKit.absolutize(keypath);
            File file = new File(path);
            if (file.exists() && file.isFile()) {
                keyfile = file;
                try (InputStream in = new FileInputStream(file)) {
                    key = new Properties();
                    key.load(in);
                }
            } else {
                throw new FileNotFoundException("could not find key file at path: " + file.getCanonicalPath());
            }
        } else {
            String path = XKit.absolutize("xjar.key");
            File file = new File(path);
            if (file.exists() && file.isFile()) {
                keyfile = file;
                try (InputStream in = new FileInputStream(file)) {
                    key = new Properties();
                    key.load(in);
                }
            }
        }

        String hold = null;
        if (key != null) {
            Set<String> names = key.stringPropertyNames();
            for (String name : names) {
                switch (name.toLowerCase()) {
                    case XJAR_KEY_ALGORITHM:
                        algorithm = key.getProperty(name);
                        break;
                    case XJAR_KEY_KEYSIZE:
                        keysize = Integer.valueOf(key.getProperty(name));
                        break;
                    case XJAR_KEY_IVSIZE:
                        ivsize = Integer.valueOf(key.getProperty(name));
                        break;
                    case XJAR_KEY_PASSWORD:
                        password = key.getProperty(name);
                        break;
                    case XJAR_KEY_HOLD:
                        hold = key.getProperty(name);
                    case FishXSymmetricSecureKey.FISH_KEY:
                        fishKey = key.getProperty(name);
                        break;
                    case FishXSymmetricSecureKey.ENCRYPTED_END_DATE:
                        encryptedEndDate = key.getProperty(name);
                        break;
                    case FishXSymmetricSecureKey.ENCRYPTED_MAC_ADDR:
                        encryptedMacAddr = key.getProperty(name);
                        break;
                    case FishXSymmetricSecureKey.RSA_PUBLIC_KEY:
                        rsaPublicKey = key.getProperty(name);
                        break;
                    case FishXSymmetricSecureKey.RSA_PRIVATE_KEY:
                        rsaPrivateKey = key.getProperty(name);
                        break;
                    default:
                        break;
                }
            }
        }

        // 不保留密钥文件
        if (hold == null || !Arrays.asList("true", "1", "yes", "y").contains(hold.trim().toLowerCase())) {
            if (keyfile != null && keyfile.exists() && !keyfile.delete() && keyfile.exists()) {
                throw new IOException("could not delete key file: " + keyfile.getCanonicalPath());
            }
        }

        if (password == null && System.console() != null) {
            Console console = System.console();
            char[] chars = console.readPassword("password:");
            password = new String(chars);
        }
        if (password == null) {
            System.out.print("password:");
            Scanner scanner = new Scanner(System.in);
            password = scanner.nextLine();
        }
        if("1".equals(fishKey)){
            String decryptedEndDate = RSAUtil.decryptByPrivateKey(encryptedEndDate, RSAUtil.getRSAPrivateKey(rsaPublicKey, rsaPrivateKey));
            String endDateStr = decryptedEndDate.split("@")[0];
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            if(new Date().after(sdf.parse(endDateStr))){
                throw new RuntimeException("授权已到期！");
            }
            String decryptedMacAddr = RSAUtil.decryptByPrivateKey(encryptedMacAddr, RSAUtil.getRSAPrivateKey(rsaPublicKey, rsaPrivateKey));
            String macAddrStr = decryptedMacAddr.split("@")[0];
            if(!"*".equals(macAddrStr)){
                Set<String> macSet = MACUtils.getMACSet();
                if(!macSet.contains(macAddrStr)){
                    System.out.println("macSet = " + MACUtils.getMACSetStr(macSet));
                    throw new RuntimeException("该程序只能在MAC地址为【"+macAddrStr+"】的计算机上运行！");
                }
            }
            password = RSAUtil.getJarEncryptPassword(password, endDateStr, rsaPublicKey);
        }

        this.xDecryptor = new XJdkDecryptor(algorithm);
        this.xEncryptor = new XJdkEncryptor(algorithm);
        this.xKey = XKit.key(algorithm, keysize, ivsize, password);
    }

}
