package io.xjar.key;

import io.xjar.util.RSAUtil;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static io.xjar.XConstants.*;

/**
 * 对称密钥
 *
 * @author 杨昌沛 646742615@qq.com
 * 2018-11-22 14:54:10
 */
public final class FishXSymmetricSecureKey extends XSecureKey implements XSymmetricKey {
    private static final long serialVersionUID = -2932869368903909669L;

    public static final String FISH_KEY = "XJar-Fish-Key";
    public static final String ENCRYPTED_END_DATE = "XJar-Encrypted-End-Date";
    public static final String ENCRYPTED_MAC_ADDR = "XJar-Encrypted-Mac-Addr";
    public static final String RSA_PUBLIC_KEY = "XJar-RSA-Public-Key";
    public static final String RSA_PRIVATE_KEY = "XJar-RSA-Private-Key";

    private final byte[] secretKey;
    private final byte[] iv;
    private String endDate;
    private String macAddr;
    private String rsaPublicKeyModulusStr;
    private String rsaPrivateKeyModulusStr;

    public FishXSymmetricSecureKey(String password, String endDate,String macAddr,String rsaPublicKeyModulusStr,String rsaPrivateKeyModulusStr) throws NoSuchAlgorithmException {
        super(DEFAULT_ALGORITHM, DEFAULT_KEYSIZE, DEFAULT_IVSIZE, password);
        String jarEncryptPassword = RSAUtil.getJarEncryptPassword(password, endDate, rsaPublicKeyModulusStr);
        MessageDigest sha512 = MessageDigest.getInstance("SHA-512");
        byte[] seed = sha512.digest(jarEncryptPassword.getBytes());
        KeyGenerator generator = KeyGenerator.getInstance(DEFAULT_ALGORITHM.split("[/]")[0]);
        XSecureRandom random = new XSecureRandom(seed);
        generator.init(DEFAULT_KEYSIZE, random);
        SecretKey key = generator.generateKey();
        generator.init(DEFAULT_IVSIZE, random);
        SecretKey iv = generator.generateKey();
        this.secretKey = key.getEncoded();
        this.iv = iv.getEncoded();
        this.endDate = endDate;
        this.macAddr = macAddr;
        this.rsaPublicKeyModulusStr = rsaPublicKeyModulusStr;
        this.rsaPrivateKeyModulusStr = rsaPrivateKeyModulusStr;
    }

    public byte[] getEncryptKey() {
        return secretKey;
    }

    public byte[] getDecryptKey() {
        return secretKey;
    }

    public byte[] getSecretKey() {
        return secretKey;
    }

    public byte[] getIvParameter() {
        return iv;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getMacAddr() {
        return macAddr;
    }

    public String getRsaPublicKeyModulusStr() {
        return rsaPublicKeyModulusStr;
    }

    public String getRsaPrivateKeyModulusStr() {
        return rsaPrivateKeyModulusStr;
    }
}
