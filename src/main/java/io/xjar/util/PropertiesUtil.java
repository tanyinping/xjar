package io.xjar.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author fish
 * @since 2020/4/5 4:22 下午
 */
public class PropertiesUtil {

    public static Properties loadProperties(String fileName) {
        Properties pro = new Properties();
        try {
            InputStream in = PropertiesUtil.class.getResourceAsStream(fileName);
            pro.load(in);
            in.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return pro;
    }
}
