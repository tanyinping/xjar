package io.xjar.util;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

/**
 * @author fish
 * @since 2020/11/12 10:49 上午
 */
public class MACUtils {

    public static Set<String> getMACSet() {
        Set<String> macSet = new HashSet<>();
        Enumeration<NetworkInterface> el;
        try {
            el = NetworkInterface.getNetworkInterfaces();
            while (el.hasMoreElements()) {
                NetworkInterface networkInterface = el.nextElement();
                byte[] mac = networkInterface.getHardwareAddress();
                if (mac == null)
                    continue;
                String mac_s = hexByte(mac[0]) + "-" + hexByte(mac[1]) + "-"
                        + hexByte(mac[2]) + "-" + hexByte(mac[3]) + "-"
                        + hexByte(mac[4]) + "-" + hexByte(mac[5]);
                macSet.add(mac_s.toLowerCase());
            }
        } catch (SocketException e1) {
            e1.printStackTrace();
        }
        return macSet;
    }

    public static String hexByte(byte b) {
        String s = "000000" + Integer.toHexString(b);
        return s.substring(s.length() - 2);
    }

    public static String getMACSetStr(Set<String> macSet){
        StringBuilder stringBuilder = new StringBuilder();
        if(macSet!=null && !macSet.isEmpty()){
            for(String mac : macSet){
                stringBuilder.append(mac).append(",");
            }
            stringBuilder.subSequence(0,stringBuilder.length()-1);
        }
        return stringBuilder.toString();
    }
}
