package io.xjar.util;

import javax.crypto.Cipher;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;

public class RSAUtil {

    private static final String CIPHER_MODEL="RSA";

    public static KeyPair generateRSAKeyPair() {
        KeyPairGenerator keyPairGen;
        KeyPair keyPair = null;
        try {
            keyPairGen = KeyPairGenerator.getInstance("RSA");
            keyPairGen.initialize(1024);
            keyPair = keyPairGen.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return keyPair;
    }

    public static String encryptByPublicKey(String encryptStr, RSAPublicKey rsaPublicKey)  {
        try {
            Cipher cipher = Cipher.getInstance(CIPHER_MODEL);
            cipher.init(Cipher.ENCRYPT_MODE, rsaPublicKey);
            byte[] enBytes = cipher.doFinal(encryptStr.getBytes());
            BigInteger bigInteger = new BigInteger(enBytes);
            return bigInteger.toString(16).toUpperCase();
        }catch (Exception ex){
            throw new RuntimeException(ex);
        }
    }

    public static String decryptByPrivateKey(String decryptStr, RSAPrivateKey rsaPrivateKey) throws Exception {
        Cipher cipher = Cipher.getInstance(CIPHER_MODEL);
        cipher.init(Cipher.DECRYPT_MODE,rsaPrivateKey);
        return new String(cipher.doFinal(new BigInteger(decryptStr,16).toByteArray()));
    }

    public static String getRSAPublicKeyModulusStr(KeyPair keyPair){
        String resPublicKeyModulusStr = ((RSAPublicKey)keyPair.getPublic()).getModulus().toString(16).toUpperCase();
        return resPublicKeyModulusStr;
    }

    public static String getRSAPrivateKeyModulusStr(KeyPair keyPair){
        String resPrivateKeyModulusStr = ((RSAPrivateKey) keyPair.getPrivate()).getPrivateExponent().toString(16).toUpperCase();
        return resPrivateKeyModulusStr;
    }

    public static RSAPublicKey getRSAPublicKey(String resPublicKeyModulusStr){
        KeyFactory kf;
        RSAPublicKey publicKey=null;
        try {
            kf = KeyFactory.getInstance("RSA");
            RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(resPublicKeyModulusStr, 16), new BigInteger("10001", 16));
            publicKey=(RSAPublicKey)kf.generatePublic(spec);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return publicKey;
    }

    public static RSAPrivateKey getRSAPrivateKey(String resPublicKeyModulusStr, String resPrivateKeyModulusStr){
        KeyFactory kf;
        RSAPrivateKey privateKey=null;
        try {
            kf = KeyFactory.getInstance("RSA");
            RSAPrivateKeySpec spec = new RSAPrivateKeySpec(new BigInteger(resPublicKeyModulusStr, 16), new BigInteger(resPrivateKeyModulusStr, 16));
            privateKey=(RSAPrivateKey)kf.generatePrivate(spec);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return privateKey;
    }

    public static String getJarEncryptPassword(String password, String endDate, String rsaPublicKeyModulusStr){
        String sourceStr = endDate+"@"+password+"@"+rsaPublicKeyModulusStr;
        return MD5Utils.md5(sourceStr);
    }

    public static String getEncryptedEndDate(String endDate,String rsaPublicKeyModulusStr){
        RSAPublicKey rsaPublicKey =RSAUtil.getRSAPublicKey(rsaPublicKeyModulusStr);
        return RSAUtil.encryptByPublicKey(endDate+"@"+System.currentTimeMillis(),rsaPublicKey);
    }

    public static String getEncryptedMacAddr(String macAddr,String rsaPublicKeyModulusStr){
        if(macAddr==null || macAddr.isEmpty()){
            macAddr = "*";
        }
        RSAPublicKey rsaPublicKey =RSAUtil.getRSAPublicKey(rsaPublicKeyModulusStr);
        return RSAUtil.encryptByPublicKey(macAddr+"@"+System.currentTimeMillis(),rsaPublicKey);
    }
}
