package io;

/**
 * @author fish
 * @since 2020/4/5 5:57 下午
 */
public class FishXKey {

    private String password;

    private String endDate;

    private String rsaPublicKeyModulusStr;

    private String rsaPrivateKeyModulusStr;

    public FishXKey(String password, String endDate,
                    String rsaPublicKeyModulusStr, String rsaPrivateKeyModulusStr) {
        this.password = password;
        this.endDate = endDate;
        this.rsaPublicKeyModulusStr = rsaPublicKeyModulusStr;
        this.rsaPrivateKeyModulusStr = rsaPrivateKeyModulusStr;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getRsaPublicKeyModulusStr() {
        return rsaPublicKeyModulusStr;
    }

    public void setRsaPublicKeyModulusStr(String rsaPublicKeyModulusStr) {
        this.rsaPublicKeyModulusStr = rsaPublicKeyModulusStr;
    }

    public String getRsaPrivateKeyModulusStr() {
        return rsaPrivateKeyModulusStr;
    }

    public void setRsaPrivateKeyModulusStr(String rsaPrivateKeyModulusStr) {
        this.rsaPrivateKeyModulusStr = rsaPrivateKeyModulusStr;
    }

}
